package test;

import java.util.Date;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.Annotation.Tag;
import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.MediaObject;
import fi.laji.datawarehouse.etl.models.dw.MediaObject.MediaType;
import fi.laji.datawarehouse.etl.models.dw.Quality;
import fi.laji.datawarehouse.etl.models.dw.Quality.Issue;
import fi.laji.datawarehouse.etl.models.dw.Quality.Source;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.Unit.Sex;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class TestingModels {

	private static int seq = 1;

	@Test
	public void test() throws Exception {
		Qname sourceId = Qname.fromURI("http://tun.fi/KE.3");
		Qname collectionId = new Qname("HR.1234"); // http://tun.fi/HR.1234
		Qname documentId = Qname.fromURI(collectionId.toURI() + "/1234"); // http://tun.fi/HR.1234/1234  

		DwRoot root = new DwRoot(documentId, sourceId);
		root.setCollectionId(new Qname("HR.1234")); // http://tun.fi/HR.1234

		Document publicDoc = root.createPublicDocument();
		// Document privateDoc = root.createPrivateDocument(); // If needed, generate an alternative version (with more data) for private side of the FinBIF DW

		publicDoc.addEditorUserId("myid:1234");
		publicDoc.addKeyword("projektiCZY:1234");
		publicDoc.createQuality().setIssue(new Quality(Issue.INVALID_CREATED_DATE, Source.ORIGINAL_DOCUMENT)); // original db has some quality markings -> 

		publicDoc.addFact("someinfo", "somevalue");
		publicDoc.addFact("otherinfo", "othervalue");

		Gathering gathering = parseGathering(generateId(root));

		String myDatabaseOccurrenceId = "xyz12345";
		Qname unitId = Qname.fromURI(collectionId.toURI() + "/" + myDatabaseOccurrenceId); // It is important that the id of an occurrence does not change between (re)loads of the data
		Unit unit = parseUnit(unitId); 

		publicDoc.addGathering(gathering);
		gathering.addUnit(unit);

		JSONObject json = ModelToJson.rootToJson(root);
		String data = json.toString();
		System.out.println(data);
		System.out.println(json.beautify());
	}

	private Unit parseUnit(Qname unitId) throws CriticalParseFailure {
		Unit unit = new Unit(unitId);
		unit.setTaxonVerbatim("susi");
		unit.setReportedTaxonId(new Qname("MX.46549")); // http://tun.fi/MX.46549 == Canis lupus - susi
		unit.setDet("Person name who has done the identification");

		unit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		unit.setLifeStage(LifeStage.ADULT);
		unit.setSex(Sex.FEMALE);
		unit.setAbundanceString("1");
		unit.setNotes("Jalanjäljet");

		try {
			MediaObject media = new MediaObject(MediaType.IMAGE, "https://my.media.ex/123.jpg");
			media.setThumbnailURL("https://my.media.ex/123_th.jpg");
			media.setAuthor("Photographer name");
			media.setCaption("caption");
			media.setLicenseIdUsingQname(new Qname("http://tun.fi/MZ.intellectualRightsCC0-4.0")); // http://schema.laji.fi/alt/MZ.intellectualRightsEnum
			unit.addMedia(media);
		} catch (DataValidationException e) {

		}

		unit.addFact("someinfo", "somevalue");

		// Quality control information from the original source - documentation about meaning/effect to follow later
		unit.addSourceTag(Tag.CHECK_COORDINATES); 
		unit.addSourceTag(Tag.EXPERT_TAG_UNCERTAIN);

		unit.setBreedingSite(true); // nest/etc - for some species nest sites have been defined to require automatic coarsing in FinBIF DB, so if this info is available it is valuable
		unit.setWild(false); // If captive / cultivated -- non-wild occurrences are not show by default in FinBIF

		// unit.setInterpretations(interpretations); Don't touch -- interpretations are for the DW
		// unit.setLinkings(linkings); Don't touch -- dynamic linkings (to taxonomy) are done by the DW

		return unit;
	}

	private Gathering parseGathering(Qname gatheringId) throws CriticalParseFailure, DataValidationException {
		Gathering gathering = new Gathering(gatheringId);

		gathering.setEventDate(new DateRange(new Date())); // java.util.Date - initialize correct date using your prefferred method
		// gathering.setEventDate(new DateRange(new Date(start), new Date(end))); // date range

		gathering.setHourBegin(22); // time of day in 24 hour system
		gathering.setHourEnd(23);

		gathering.setMunicipality("Sastamala");

		gathering.setGeo(Geo.fromGeoJSON(generatedGeoJson())); // Geometries from GeoJSON

		// gathering.setGeo(Geo.fromWKT("POINT(27.2 62.3)", Type.WGS84); // You can also use WKT

		// gathering.setCoordinates(new Coordinates(60.1, 30.4, Type.WGS84)); You can also define a single point or a bounding box
		// gathering.setCoordinatesVerbatim("60,1 N 30,4 E");

		gathering.addFact("LumenSyvyys", "32.2");

		gathering.addTeamMember("Kille Kiikari");
		gathering.addObserverUserId("myid:1234");
		gathering.addTeamMember("Tuija Tutkija");
		gathering.addObserverUserId("myid:5678");

		// gathering.setConversions(conversions); Don't touch - conversions are done by the dw
		// gathering.setInterpretations(interpretations); Don't touch - interpretations are done by the dw

		return gathering;
	}

	private JSONObject generatedGeoJson() {
		String myValidGeoJson = "" + 
				"{ " +
				"  \"crs\": \"WGS84\", " +  // Must add "crs" expansion parameter to GeoJSON - this is not in GeoJSON spec; valid values are WGS84, EUREF, YKJ
				"  \"type\": \"FeatureCollection\", " +
				"  \"features\": [ " +
				"    { " +
				"      \"type\": \"Feature\", " +
				"      \"properties\": {}, " +
				"      \"geometry\": { " +
				"        \"type\": \"Point\", " +
				"        \"coordinates\": [ " + 
				"          22.873535156249996, " + 
				"          61.380936033590665 " +
				"        ] " +
				"      } " +
				"    } " +
				"  ] " +
				"}";
		return new JSONObject(myValidGeoJson);
	}

	private Qname generateId(DwRoot root) {
		return new Qname(root.getDocumentId().toString()+"#"+(seq++));
	}

}
