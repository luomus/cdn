package test;

import java.util.Date;

import org.junit.Test;

import fi.laji.datawarehouse.etl.models.dw.DateRange;
import fi.laji.datawarehouse.etl.models.dw.Document;
import fi.laji.datawarehouse.etl.models.dw.DwRoot;
import fi.laji.datawarehouse.etl.models.dw.Gathering;
import fi.laji.datawarehouse.etl.models.dw.Unit;
import fi.laji.datawarehouse.etl.models.dw.Unit.LifeStage;
import fi.laji.datawarehouse.etl.models.dw.Unit.RecordBasis;
import fi.laji.datawarehouse.etl.models.dw.geo.Geo;
import fi.laji.datawarehouse.etl.models.exceptions.CriticalParseFailure;
import fi.laji.datawarehouse.etl.models.exceptions.DataValidationException;
import fi.laji.datawarehouse.etl.utils.ModelToJson;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.json.JSONObject;

public class TestingModels {

	private static int seq = 1;

	@Test
	public void test() throws Exception {
		Qname sourceId = Qname.fromURI("http://tun.fi/KE.3");
		Qname documentId = Qname.fromURI(sourceId.toURI() + "/1234"); // http://tun.fi/KE.3/1234  
		DwRoot root = new DwRoot(documentId, sourceId);
		root.setCollectionId(new Qname("HR.1234")); // http://tun.fi/HR.1234

		Document publicDoc = root.createPublicDocument();
		publicDoc.addEditorUserId("myid:1234");

		Gathering gathering = parseGathering(generateId(root));
		Unit unit = parseUnit(generateId(root));

		publicDoc.addGathering(gathering);
		gathering.addUnit(unit);

		JSONObject json = ModelToJson.rootToJson(root);
		String data = json.toString();
		System.out.println(data);
		System.out.println(json.beautify());
	}

	private Unit parseUnit(Qname unitId) throws CriticalParseFailure {
		Unit unit = new Unit(unitId);
		unit.setTaxonVerbatim("susi");
		unit.setRecordBasis(RecordBasis.HUMAN_OBSERVATION_INDIRECT);
		unit.setNotes("Jalanjäljet");
		unit.setLifeStage(LifeStage.ADULT);
		unit.setAbundanceString("1");
		return unit;
	}

	private Gathering parseGathering(Qname gatheringId) throws CriticalParseFailure, DataValidationException {
		Gathering gathering = new Gathering(gatheringId);

		gathering.setEventDate(new DateRange(new Date())); // java.util.Date - initialize correct date using your prefferred method
		// gathering.setEventDate(new DateRange(new Date(start), new Date(end))); // date range

		gathering.setHourBegin(22); // time of day in 24 hour system
		gathering.setHourEnd(23);

		gathering.setMunicipality("Sastamala");

		gathering.setGeo(Geo.fromGeoJSON(generatedGeoJson())); // Geometries from GeoJSON

		// gathering.setGeo(Geo.fromWKT("POINT(27.2 62.3)", Type.WGS84); // You can also use WKT

		// gathering.setCoordinates(new Coordinates(60.1, 30.4, Type.WGS84)); You can also define a single point or a bounding box
		// gathering.setCoordinatesVerbatim("60,1 N 30,4 E");

		gathering.addFact("LumenSyvyys", "32.2");
		return gathering;
	}

	private JSONObject generatedGeoJson() {
		String myValidGeoJson = "" + 
				"{ " +
				"  \"crs\": \"WGS84\", " +  // Must add "crs" expansion parameter to GeoJSON - this is not in GeoJSON spec; valid values are WGS84, EUREF, YKJ
				"  \"type\": \"FeatureCollection\", " +
				"  \"features\": [ " +
				"    { " +
				"      \"type\": \"Feature\", " +
				"      \"properties\": {}, " +
				"      \"geometry\": { " +
				"        \"type\": \"Point\", " +
				"        \"coordinates\": [ " + 
				"          22.873535156249996, " + 
				"          61.380936033590665 " +
				"        ] " +
				"      } " +
				"    } " +
				"  ] " +
				"}";
		return new JSONObject(myValidGeoJson);
	}

	private Qname generateId(DwRoot root) {
		return new Qname(root.getDocumentId().toString()+"#"+(seq++));
	}

}
