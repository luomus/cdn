# cdn.laji.fi #

This is the place to store external libraries that you don't want to include in your own repository
Content of dist folder is exposed publically over the cdn.laji.fi

### Naming conventions ###

in dist folder pleace use folloing convention unless there is strong reason not to
`/dist/<package name>/<version>/<anything you like>`

so for example to serve bootstarp over this we'd use
`/dist/bootstrap/3.3.7/*`

### Deployment guidelines ###

Add files to repo following the guidelines set above. Push changes and wait for a while to see the changes online.
You don't need to add gziped files. Update script on the server is inflating all the files when it's detecting changes.

